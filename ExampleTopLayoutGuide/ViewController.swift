//
//  ViewController.swift
//  ExampleTopLayoutGuide
//
//  Created by Utkarsh Rathor on 06/01/16.
//  Copyright © 2016 Utkarsh Rathor. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        print("Calling the function starts") ;
        let viewFault : UIView = UIView() ;
        let viewCorrect : UIView = UIView() ;
        
        viewFault.backgroundColor = UIColor.redColor() ;
        
        viewCorrect.backgroundColor = UIColor.blueColor() ;
        
        
        self.view.addSubview(viewCorrect) ;
        
        self.view.addSubview(viewFault) ;
        
        
        viewFault.translatesAutoresizingMaskIntoConstraints = false ;
        viewCorrect.translatesAutoresizingMaskIntoConstraints = false ;
        NSLayoutConstraint.activateConstraints([
                viewFault.topAnchor.constraintEqualToAnchor(self.view.topAnchor) ,
            viewFault.heightAnchor.constraintEqualToConstant(80) ,
            viewFault.widthAnchor.constraintEqualToConstant(80) ,
            viewFault.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor),
            
            
            viewCorrect.topAnchor.constraintEqualToAnchor(self.topLayoutGuide.bottomAnchor) ,
            viewCorrect.heightAnchor.constraintEqualToConstant(80) ,
            viewCorrect.widthAnchor.constraintEqualToConstant(80) ,
            viewCorrect.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor)

            
            ]) ;
        
        print("Calling the function ends") ;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

